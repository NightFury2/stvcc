require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST,
  apiPort: process.env.APIPORT,
  app: {
    title: 'Stvcoc',
    description: 'Stavropol Collage of Communication',
    head: {
      titleTemplate: 'Stvcoc: %s',
      meta: [
        {name: 'description', content: 'Stavropol Collage of Communication'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'Stavropol Collage of Communication'},
        {property: 'og:image', content: 'https://react-redux.herokuapp.com/logo.jpg'},
        {property: 'og:locale', content: 'ru_RU'},
        {property: 'og:title', content: 'Stavropol Collage of Communication'},
        {property: 'og:description', content: 'Stavropol Collage of Communication'},
        {property: 'og:card', content: 'summary'},
        {property: 'og:site', content: '@nightfury2'},
        {property: 'og:creator', content: '@nightfury2'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  },

}, environment);
